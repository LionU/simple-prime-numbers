#! /usr/bin/env python
# -*- coding: utf-8 -*-


''' 
	Теорема Рабина. \n
	 \n
	 Пусть $m$ — нечётное число большее 1. \n 
	 Число $m-1$ однозначно представляется в виде $m-1 = 2^s \\cdot t$, где $t$ нечётно. \n
	 Целое число $a$, $1 < a < m$, называется свидетелем простоты числа $m$, если выполняются два условия:\n
	 $m$ не делится на $a$;
	 $a^t\\equiv 1\\pmod m$ или существует целое $k$, $0 <= k<s$, такое, что $ a^{2^kt}\\equiv -1\\pmod m$ \n
	 Теорема Рабина утверждает, что составное нечётное число $m$ имеет \n
	 не более $\\phi(m)/4$ различных свидетелей простоты, \n
	 где $\\phi(m)$ — функция Эйлера.\n
	 
'''


## импортируем стандартный модуль работы со случайными числами и тандартный модуль работы c математическими фунуциями
import random 
import math

## импортируем вспомогательный модуль модуль 
import Util

'''
	\overload [powMod]
	псевдоним для src.Util.powMod из src.Util
'''
powMod = Util.powMod
	


def makeSieve(n):
	'''
	Решето Эратосфена
	
	создадим последовательность длинной n 
	'''
	sieve = range(n)

	for count in range(2, int(math.sqrt(n))):
		if sieve[count] == 0:
			continue
		x = sieve[count] * 2
		while x < len(sieve):
			sieve[x] = 0
			x += sieve[count]
	sieve = [x for x in sieve[2:] if x]
	''' импользовали генератор списков 
	
	sieve = список из всех таких X которые входят в sieve и не равны 0 '''
	return sieve


sieve = makeSieve(1000) 
'''Задали последовательность 1000 '''

def MillerRabinTest1( number , iterations = 10):

	'''  Тест Миллера-Рабина
	
		 Алгоритм частично взят с \n
		 http://geo.web.ru/db/msg.html?mid=1161287&uri=node162.html#tests \n
	'''
	
	## если number <= 0 вызываем исключение
	if (number <= 0):
		raise ValueError("MillerRabinTest( %s , iterations)"%(number))
	## если 0 < number <= 2 оно простое
	if (number <= 2):
		return True;
	## если четное -- оно составное
	if (number % 2 == 0):
		return False;
	if (iterations == None):
		## Рекомендуется брать число раундов порядка величины log2(m), где m — проверяемое число.
		iterations = math.log(number)/math.log(2)
	## служебные переменные
	## numder - 1 == u * 2 ^ (t)
	s  ,t = 0, 1.1

	while (int(t) % 2 == 0 ) or (t - int ( t ) !=  0.0):
		s+=1
		t=(number-1)/(2**s)
		# - print  'MillerRabinTest 1'
		
	cnt = 0
	## в указанном алгоритме это случайное b, 1 < b < n; \n
	## для поиска псевдослучайного числа используется
	## алгоритм Вичмана-Хилла
	witness = random.randrange(1,number+1) 
	while( cnt < iterations ):
		# - print  ('MillerRabinTest 2')
		# - print  "MillerRabinTest 2", witness - 1
		# - print  "MillerRabinTest 2", number
		# - print  "MillerRabinTest 2",(witness - 1) < number
		# - print  "MillerRabinTest 2 (witness - 1)", witness - 1
		# - print  "MillerRabinTest 2 (witness - 1)/number", (witness - 1)/number
		# - print  "MillerRabinTest 2 (witness - 1)%number", (witness - 1)%number
		while((witness - 1) % number == 0):
			# - print  '2.1 -- '
			## ТУТ проверим удовлетворяет ли условию 1) b == 1(mod n)
			witness=random.randrange(1,number)	
			# - print  '2.1'
		## witness в степени t	
		# - print  '2.2', witness, t
		# - print  ">>>>>>>>>>>>>>>", number-1, s,t, t*2**s
		#x = ( witness** t ) % number
		x = powMod(witness, t, number)
		# - print  '2.3'
		for i in xrange( 1, s+1 ):
			# - print  '2.2'
			x = ( x * x ) % number
		if( x != 1 % number):
			return False
		cnt += 1
	return True


def MillerRabinTest2(number, iterations=7):
	'''  Тест Миллера-Рабина
	
		Тоже самое что и src.MillerRabin.MillerRabinTest1 
		но более разумная и эффективная реализация
	'''

	## ПРОВЕРКИ
	if (number <= 0):
		raise ValueError("MillerRabinTest( %s , iterations)"%(number))
	for x in sieve:
		if x >= number: return True
		if number % x == 0: return False
	if (number % 2 == 0):
		return False;

	## НАЙДЕМ S, T	
	s, t = number-1, 0
	while s % 2 == 0:
		s, t = s/2, t+1

	## ОСНОВНОЙ ЦИКЛ		
	a = 2 
	for count in range(iterations):
		v = powMod(a, s, number)
		if v==1:
			continue
		i = 0
		while v != number-1:
			if i == t-1:
				return False
			else:
				v, i = powMod(v, 2, number), i+1
		a = random.randrange(2, number)
	return True


def test():
	k = 9412811077447068167881968240962335651367355368642261920624771321
	if MillerRabinTest1(k) == False:
		print  "old 0"
	else:
		print  "old 1"
	if MillerRabinTest2(k) == False:
		print  "new 0"
	else:
		print  "new 1"


## псевдоним для  src.MillerRabin.MillerRabinTest2 из src.MillerRabin
MillerRabinTest = MillerRabinTest2



if (__name__ == '__main__'):
	test()

	


	