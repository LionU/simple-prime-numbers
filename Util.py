#! /usr/bin/env python
# -*- coding: utf-8 -*-


# строка документация 
''' 
		http://gmpy.sourceforge.net/
		\n
		Библиотека GMP используется для работы над знаковыми целыми, 
		рациональными числами и числами с плавающей точкой. 
		Главная особенность библиотеки — разрядность чисел (precision) практически неограничена. 
		Поэтому основная область применения — криптография, 
		компьютерные алгебраические вычисления и др. 

'''
try:
	import gmpy
	# Попытаемся загрузить
	# GMPY (http://gmpy.sourceforge.net/)
	# модуль для ускорения арифметических операций

	gmpyLoaded = True
except ImportError:
	# если ее нет реализуем сами то что нужно
	gmpyLoaded = False

def invMod(a, b):
	'''
	не ( A mod B)
		Возвращает обратное значение от ( A mod B) \n
		используется расширенный алгоритм эвклида \n
		нужен для powMod_SELF
	'''
	c, d = a, b
	uc, ud = 1, 0
	while c != 0:
		q = d / c
		c, d = d-(q*c), c
		uc, ud = ud - (q * uc), uc
	if d == 1:
		return ud % b
	return 0

def fpow(base, power):
	base = gmpy.mpz(base)
	power = gmpy.mpz(power)
	return base**power
	#result = gmpy.mpz.powm(base, power)
	

def powMod_GMPY(base, power, modulus):
	''' 
	A^B(mod N) 

	возведение в степень по модулю
	из GMP
	'''
	base = gmpy.mpz(base)
	power = gmpy.mpz(power)
	modulus = gmpy.mpz(modulus)
	result = pow(base, power, modulus)
	return long(result)


def powMod_SELF(base, power, modulus):
	''' 
	A^B(mod N) 
	'''
	nBitScan = 5
	negativeResult = False
	if (power < 0):
		power *= -1
		negativeResult = True

	exp2 = 2**nBitScan
	mask = exp2 - 1

	'''Заносим степени в списки '''
	nibbles = None
	while power:
		nibbles = int(power & mask), nibbles
		power = power >> nBitScan

	''' Создадим таблицу степеней '''
	lowPowers = [1]
	for i in xrange(1, exp2):
		lowPowers.append((lowPowers[i-1] * base) % modulus)

	''' для возводения в степень первый огрызок, смотрим его в таблице '''
	nib, nibbles = nibbles
	prod = lowPowers[nib]

	'''для остальных nBitScan, base^nibble'''
	while nibbles:
		nib, nibbles = nibbles
		for i in xrange(nBitScan):
			prod = (prod * prod) % modulus
		if nib: prod = (prod * lowPowers[nib]) % modulus

	if negativeResult:
		prodInv = invMod(prod, modulus)
		if (prod * prodInv) % modulus != 1:
			''' проверка корректности '''
			raise AssertionError()
		return prodInv
	return prod
	
	
def test():
	print powMod_GMPY(10, 2, 1000), powMod_SELF(10, 2, 1000)
	print powMod_GMPY(9412811077447068167881968240962335651367355368642261920624771321, 625004788939, 436309)
	print powMod_GMPY(9412811077447068167881968240962335651367355368642261920624771321, 625004788939, 4363090)

## псевдоним для  src.Util.powMod_GMPY или src.powMod_SELF.powMod_GMPY из src.powMod_GMPY 
if gmpyLoaded:
	powMod = powMod_GMPY
else:
	powMod = powMod_SELF
	
	
	
if (__name__ == '__main__'):
	print '!'
	test()

	
