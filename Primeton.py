#! /usr/bin/env python
# -*- coding: utf-8 -*-


''' 
	Построение больших простых чисел
	
	Построение больших простых чисел с 
	использованием частичного разложения на множители 
	
'''


# *********************************************************************
# IMPORTS
# *********************************************************************
## импортируем стандартный модуль работы со случайными числами
import random 

## описание фунцций поиска НОД
import GCD

## описание теста Миллера-Рабина 
import MillerRabin

## описание фунцций powMod
import Util


## псевдоним для src.GCD.euklid из src.GCD 
gcd = GCD.euklid

## псевдоним для  src.MillerRabin.MillerRabinTest2 из src.MillerRabin
isPrime = MillerRabin.MillerRabinTest

## псевдоним для  src.Util.powMod из src.Util
powMod = Util.powMod


def PrimeNumber(seed = 71, check_beg = False, check_end = False):
	try:
		if(check_beg):
			isPrime(seed)
	except:
		seed = 3

	''' 
		в указанном алгоритме это случайное b, 1 < b < n; \n
		для поиска псевдослучайного числа используется
		алгоритм Вичмана-Хилла
		ищем четное
	'''
	randomizer = random.randrange(2,4*seed+2, 2) 
	guess_result = seed * randomizer + 1
	while ( not isPrime( guess_result ) ):
		randomizer = random.randrange(2,4*seed+2, 2) 
		guess_result = seed * randomizer + 1
	
	result = guess_result
	
	checker = random.randrange(1, result) 
	'''условия'''
	condition1 = False;
	condition2 = False;
	condition3 = False;
	## проверка 1
	if ( powMod(checker , ( result - 1 ), result ) == 1 % result):
		## a^{N-1} = 1(mod N),
		condition1 = True;

	## проверка 2.1
	if(gcd( (checker), result) == 1):
		## НОД(a^R-1, N)=1.
		condition2 = True
	
	## проверка 2.2
	if(gcd( (checker) - 1, result) == 1):
		## НОД(a^R-1, N)=1.
		condition2 = condition2 & True
		
	## проверка 2.3
	if(gcd( (randomizer), result) == 1):
		## НОД(a^R-1, N)=1.
		condition2 = condition2 & True
		
	## проверка 2.4
	if(gcd( (randomizer) - 1, result) == 1):
		## НОД(a^R-1, N)=1.
		condition2 = condition2 & True
		
	if(check_end):
		if isPrime(result):
			condition3 = True;
	else:
			condition3 = True;


	return ((condition1 and condition2 and condition3), result)
	return {'простое ли':(condition1 and condition2 and condition3), 'значение':result}
	
def PrimeNumberSeq( length = 7, seed = 11 ):
	''' 
	Поиск последовательности простых чисел
	
	Создается список из length значений
	для поиска каждого простого числа используется фугкция PrimeNumber
	с затравной seed
	'''
	result_list = []
	''' создаем пустой список '''
	next_seed = seed
	''' for(int i = 0 ; i != length; ++i) '''
	for i in xrange(length):	
		''' затравка та же '''
		answer = PrimeNumber(seed)
		'''answer = (простота, значение)'''
		if (answer[0] == True):
			next_seed = answer[1];
			''' если простое -- добавляем в список '''
			result_list.append(next_seed)
	return result_list
	
def PrimeNumberGrowingSeq( length = 10, seed = 11 ):
	## Поиск возрастающей последовательности простых чисел

	result_list = []
	''' создаем пустой список '''
	next_seed = seed
	''' for(int i = 0 ; i != length; ++i) '''
	for i in xrange(length):
		''' затравка та новая '''
		answer = PrimeNumber(next_seed)
		'''answer = (простота, значение)'''
		if (answer[0] == True):
			next_seed = answer[1];
			''' если простое -- добавляем в список '''
			result_list.append(next_seed)
	return result_list
	
	
def test( length = 10,  fool = False, seed = 11, check_beg = False, check_end = False,):
	next_seed = seed
	for i in xrange(length):		
		answer = PrimeNumber(next_seed, check_beg, check_end)
		if(fool):
			print answer
		if (answer[0] == True):
			if(not fool):
				print answer[1]
			next_seed = answer[1];
	

# MAIN RUNs
# *************************************************
if (__name__ == '__main__'):
	test()
	
	
	