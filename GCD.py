#! /usr/bin/env python
# -*- coding: utf-8 -*-


def euklid( a, b ):
	'''
	Наибольший Общий Делитель
	
	Алгоритм Эвклида поиска НОД
	'''
	## если a или b  == 0 вызываем исключение
	if( a == 0 or b == 0 ):
		raise ValueError("GCD.euklid(%s, %s)"%(a, b))
	while( b != 0):
		a , b = b ,a % b
	return a


def euklid_recursive( a, b ):
	'''
		 Наибольший Общий Делитель (рекурсивный)

		Алгоритм Эвклида поиска НОД (рекурсивный) 
		http://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%95%D0%B2%D0%BA%D0%BB%D0%B8%D0%B4%D0%B0
	'''
	if b == 0: 
		return a
	else: 
		return gcd(b, a % b)



# ======================== ========================
# MAIN FUNCTION
# ======================== ========================
def test():
	print euklid( 1000000000000000000000 , 10 )
	print euklid( 10**100 , 10**99 )
	print euklid( 10**1000 , 10**99 )
	print euklid( 20988936657440586486151264256610222593863921 , 10**99 )
	
# MAIN RUNs

if (__name__ == '__main__'):
	test()
	
	
	